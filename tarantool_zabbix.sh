#!/bin/bash
PATH="/bin:/sbin:/usr/bin:/usr/sbin"
case "$1" in
  "")
    echo "arg need"
    exit 1
    ;;
  "discovery")
    /usr/bin/ls -1 /etc/tarantool/instances.available/ | grep -v example.lua | perl -ne 'BEGIN{print "{\"data\":["};if (/(.*)\.lua/) {push @ar, "{\"{#TARANTOOL_INSTANCE}\":\"$1\"}"};END{print join(",",@ar);print "]}"}'

    ;;
  *)
    case "$2" in
      "status")
	echo 'box.info.status' | tarantoolctl eval $1 2>&1| perl -ne 'print $1 if (/^-\s(\w+)/)'
	;;
      "connections")
	echo 'box.stat.net().CONNECTIONS' | tarantoolctl eval $1 2>&1 | perl -ne 'print $1 if (/^-\s(\d+)/)'
	;;
      "quota_used_ratio")
	echo 'box.slab.info().quota_used_ratio' | tarantoolctl eval $1 2>&1 | perl -ne 'print $1 if (/^-\s(.*?)%/)'
	;;
      "rul")
	echo 'box.info.replication' | tarantoolctl eval $1 2>&1 | perl -ne 'BEGIN{$lag=0};if (/lag:(.*)$/) {if ($lag<=abs $1){$lag=abs $1}}END{print $lag}'
	;;
      "rus")
	echo 'box.info.replication' | tarantoolctl eval $1 2>&1 | grep upstream -A 1 | perl -ne 'if (($st)=/status:\s*(\w+)/) {unless ($st eq "follow") {last;}};END{print $st;}'
	;;
      "rds")
	echo 'box.info.replication' | tarantoolctl eval $1 2>&1 | grep downstream -A 1 | perl -ne 'if (($st)=/status:\s*(\w+)/) {unless ($st eq "follow") {last;}};END{print $st;}'
	;;
    esac    
    ;;
esac
